﻿#####################################
POSITRON EMISSION TOMOGRAPHY (PET CT)
#####################################

**********
**FUSION**
**********

|image51|

   The **Fusion** function allows you to combine the series of PET and
   CT types (attach the selected PET series to the displayed CT series),
   thus linking the sites of radioactive drug concentrations with the
   anatomical patient structure. The series fusion function can be
   selected only after loading the series into the active window. The
   **Fusion** function is achieved by selecting the Fusion icon in the
   main toolbar or by pressing the **F** keyboard shortcut button.

   If there is no PET series in the open study (-ies), notification will
   appear on the screen:

|image52|

Figure - PET notification

   The ability to choose a PET series from another study allows the user
   to combine series of different studies. This function also allows the
   user to combine magnetic resonance imaging (MR) series with PET
   series.

-  Click **Fusion** button and the following pop-up window with positron
   emission tomography (PET) will appear on the screen:

|image53|

Figure - PET dialog

-  The PET series selection dialog allows the user to select PET series
   from an active study (a study that has a CT series open in the active
   viewport) or from a series of PET in other open studies.

-  In the PET series selection dialog select the PET series you want to
   combine and click on the **Start Fusion** button.

-  The merging process is seen on the screen:

|image54|

Figure - Fusion process

   Once done, the following window will appear on the screen:

|image55|

Figure - Fusion function

*****************************
**MANIPULATING FUSED SERIES**
*****************************

   The functionality of the fused series toolbar is described below.

|image56|

Figure - Fusion toolbar

===========================
1. CHANGING THE COLOR RANGE
===========================

   By default, the **Hot iron** color range is applied to the fusion
   series. However, according to the specificities of the anatomical
   body sections analyzed, different color range can be used. The choice
   of the color selection is performed by expanding the list of color
   schemes and choosing the desired color range. The selected color
   range for the fused series is automatically applied immediately after
   selection.

|image57|

Figure - Fusion color range

============================
2. CHANGING THE FUSION RATIO
============================

   In the analysis of fused series, it is important to have the ability
   to change the fusion ratio which is possible with **Transparent
   layer** button. Changing the ratio is possible by switching the ratio
   change marker to the PET or CT series. In this way, one or the other
   series is highlighted and a clearer view is provided.

|image58|

Figure - Fusion ratio

==============================
3. UPPER LAYER DATA ADJUSTMENT
==============================

   If the fusion of the series is not completely symmetrical, then the
   **Pan** tool can be used. This tool allows you to visually
   anatomically link the fused series.

|image59|

Figure - Fusion Pan

   By selecting the **Pan** tool in the fusion toolbar, only the upper
   layer (PET series) position is changed. If the **Pan** tool located
   on the main toolbar is selected, the joint position of the fusion
   series is changed in this case.

Adjust the Level/Window (contrast and brightness) of the upper layer
********************************************************************

   By choosing the **Windowing** tool in the fusion toolbar, the user
   can change the level of brightness to the upper layer (applicable to
   the PET series). If the **Windowing** tool is selected from the main
   toolbar, then the overall brightness level (PET and CT series) is
   changed.

|image60|

Figure - Fusion Windowing

Upper layer zoom function
*************************

   In the fusion toolbar, by selecting the **Zoom** tool, the system
   user can change the top layer scale (applicable to the PET series).
   If the Zoom tool is selected from the main toolbar, in this case the
   overall (PET and CT series) scaling is changed.

|image61|

Figure - Fusion Zoom

Upper layer rotation
********************

   Once the **Rotate** tool in the Fusion toolbar, the system user can
   rotate the top layer to one-degree accuracy (applicable to the PET
   series). If the **Rotate** tool is selected from the main toolbar, in
   this case the rotating image (PET and CT series) is rotated.

|image62|

Figure - Fusion Rotation

Upper layer image overlay
*************************

   If there is a mismatch between the fused series, the series images
   can be overlaid. The system user must select the **Manual
   Adjustment** button from the fusion toolbar. The overlay is possible
   by changing the image to the next (by clicking the **+** button) or
   by changing the image to the previous one (by clicking the **–**
   button).

|image63|

Figure - Fusion Manual Adjustment

Standard Uptake Value (SUV)
***************************

   The main measurement used in the fusion series is the **SUV**, the
   standard uptake value. The standard uptake value is calculated
   according to the formula:

+---------+------------------+
|         | 𝑤𝑒𝑖𝑔ℎ𝑡 𝑖𝑛        |
|         |                  |
|         | 𝑔𝑟𝑎𝑚𝑠            |
+=========+==================+
| 𝑆𝑈𝑉𝑏𝑤 = |                  |
+---------+------------------+
|         |    𝑖𝑛𝑗𝑒𝑐𝑡𝑒𝑑 𝑑𝑜𝑠𝑒 |
+---------+------------------+

The **Standard Uptake Value** is calculated by choosing **Ellipse**
measurement from the measurement list from the main toolbar (*see more
on page 38* `Measuring images <#measuring-images>`__) and marking the
location in the fused image. Three standard uptake values (average,
minimum and maximum) are provided:

|image64|

Figure - Fusion function standard uptake value

Close function
**************

   To close the window, click on **Close** or **X** icons at the end of
   the fusion toolbar.

|image65|

Figure - Fusion Close button
