﻿################
MEASURING IMAGES
################

|image66|

   **Measure** button allows you to measure the images in number of
   ways:

|image67|

Figure - Measurement tools

********
**LINE**
********

|image68|

   To measure the distance:

-  click on the **Measure** button and choose **Line** from the list

-  place the mouse cursor on the starting point from which you want to
   measure the distance.

-  click the left mouse button. Move the cursor to the end point and
   click the left mouse button once more.

-  the distance (in millimeters, or pixels in some images) will be
   displayed in yellow:

|image69|

Figure - Line measurement

-  Once **Line** measurement is active, you can place as many lines as
   you want.

*********
**ANGLE**
*********

|image70|

   **Angle measurement.** To measure an angle:

-  Position the mouse pointer on the point from which you want to
   measure the angle. Then click the left mouse button.

-  Move the pointer to the second point (the intersection point) and
   click the left mouse button again.

-  Then move the pointer to the end point and click the left mouse
   button once more.

|image71|

Figure - Angle measurement

-  Once **Angle** measurement is active, you can place as many angles as
   you want.

***************
**SHOW ANGLES**
***************

|image72|

   You can also measure an angle between any intersecting lines. To
   display the angle measurements:

-  draw intersecting lines on the image using the **Line** measurement,

-  on the Tools menu, click **Measure** button,

-  tick **Show Angles**:

|image73|

Figure - Angle measurement between intersecting lines

************
**POLYLINE**
************

|image74|

   The **Polyline** button is used to measure the perimeter of a region
   of interest. To measure the perimeter:

-  Position the mouse pointer on the point from which you want to
   measure the perimeter. Then click the left mouse button.

-  Move the cursor to the second point and click the left mouse button
   again.

-  Then move the cursor to the third, fourth, etc. points and each time
   click the left mouse button again.

-  Double-click once finished in order to see the result.

|image75|

Figure - Polyline measurement

*************
**INTENSITY**
*************

|image76|

   The **Intensity** button is used to measure the density of a CT
   image. To measure the density:

-  select **Intensity** once.

-  move the mouse cursor over the point you want.

-  the density of the point and its coordinates should be visible at the
   left bottom corner (expressed in Hounsfield units, HU):

|image77|

Figure - Intensity measurement

********
**AREA**
********

|image78|

   The **Area** button is used to measure the perimeter and the area of
   a region of interest. To measure the area:

-  Place the mouse cursor on the point from which you want to select the
   region of interest. Then click the left mouse button.

-  Move the cursor to the second point and click the left mouse button
   again.

-  Then move the cursor to the third, fourth, etc. points and each time
   click the left mouse button again.

-  When you reach the last point, click the left mouse button twice.

|image79|

Figure - Area measurement

-  The area (in square millimeters) and the perimeter (in millimeters)
   will be displayed in yellow

**********
**VOLUME**
**********

|image80|

   The **Volume** button is used to measure the volume of the object.

   In the illustration below, the object can be imagined as the
   following solid of revolution: the vertical line is the rotation
   axis, around which the left and the right curves are rotated half of
   the circle.

|image81|

Figure - Volume measurement

-  Place the mouse cursor on the starting point of the rotation axis.

-  then click the left mouse button (do not hold it) and move the cursor
   to the second point and click the left mouse button again.

-  then move the cursor to the third, fourth, etc. points of one side
   curve and each time click the left mouse button again.

-  when you reach the end point of the rotation axis, click the left
   mouse button **twice** in order to specify the height of the object.

-  move cursor to the second, third, etc. points of another side curve
   and each time click the left mouse button again.

-  when you reach the last point of the side curve, click the left mouse
   button **twice** in order finish the measurement.

*******
**VTI**
*******

|image82|

   The **VTI** *(Velocity Time Integral)* button is used to measure the
   distance over which the blood was ejected per interval of time.

-  Place the mouse cursor on the point from which you want to measure
   the velocity time integral.

-  Then click the left mouse button (do not hold it) and click the
   cursor on the second point and click the left mouse button again.

-  Then move the cursor to the third, fourth, etc. points on the blood
   velocity profile and each time click the left mouse button again.

-  When you reach the last point, click the left mouse button **twice**
   in order to end the measurement.

|image83|

Figure - VTI measurement

-  The velocity time integral is measured in centimeters. This button is
   active only for the images of "US" modality.


***********
**ELLIPSE**
***********

|image84|

   **Ellipse** is used to measure area, length, width, Min and Max
   brightness in HU units and STD measurement in cm.

|image85|

Figure - Ellipse measurement

********************
**CALIBRATION LINE**
********************

|image86|

   The **Calibration line** button is used to change the scale of
   measurement.

-  Click the Calibration button;

-  Please draw a line on an image:

|image87|

Figure - Calibration line

-  Indicate line length in millimeters in a pop-up window:

|image88|

Figure - Calibration function

-  Once the data will be entered, click **Apply** – data will appear on
   the left bottom corner of the screen:

|image89|

Figure - Calibration ratio result

**************
**COBB ANGLE**
**************

|image90|

   The **Cobb angle** button is used to measure angle between lines. To
   measure angle:

-  select **Cobb angle** measurement,

-  click on image and draw two lines,

-  the Cobb angle measure will appear on the screen,

-  you can drag lines and line points; by dragging the white dotted
   line, all lines will move simultaneously.

|image91|

Figure - Cobb angle

********
**TEXT**
********

|image92|

   The **Text** button is used to save the annotations of the
   measurements.

-  Click the **Measure** icon and choose **Text** from the list.

-  An annotation text window (white field) will appear on the screen.

-  Yellow arrow can be pointed to any place of the image with a drag and
   drop motion.

-  Yellow–border text window can be placed with a drag and drop motion
   anywhere on the image, for example, next to the measurement you want
   to add text to.

-  Press **Enter** button in order to finish your annotation

|image93|

Figure - Annotation window

*******
**ROI**
*******

|image94|

   The **ROI** (Region Of Interest) is the same as area measurement only
   without measurements.

   Annotation support is implemented according to DICOM standard. All
   measurement including text can be saved as annotation. Indication
   that annotation is associated with the study (expanded list of
   available annotations)

**************
**DELETE ALL**
**************


|image100|

   The **Delete All** button is used to remove all measurements at once.
   To remove the measurements:

-  select the image from which you want to remove all measurements

-  click **Measure**

-  select **Delete All.**
