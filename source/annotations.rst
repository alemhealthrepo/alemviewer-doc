﻿###########
ANNOTATIONS 
###########

   Annotations can be written, viewed and saved. Their presence is
   indicated by a pencil mark at the corner of small images on the left.

|image110|

Figure - Annotation mark

***********************
**TO WRITE ANNOTATION**
***********************

-  once you have made any of the measurements or manipulations of the
   study image, you will be able to write an annotation.

-  move your mouse cursor to the upper toolbar and select icon
   **Measure**.

-  click on the icon **Measure**, then select **Text** from the list.

-  select the point where you want to write an annotation text.

-  click the left mouse button on the point you have selected.

-  annotation text window will appear:

|image111|

Figure - Text window

-  click the left mouse button on the bottom annotation text window
   (white field) and now you should be able to write an annotation.

-  write an annotation for your study image.

-  click **Enter** button on your keyboard in order to finish writing
   your annotation.

|image112|

Figure - Annotation text

**********************
**TO SAVE ANNOTATION**
**********************

-  once you have written an annotation text you will be able to save it.

-  move your mouse cursor to the upper toolbar and select icon
   **Measure**.

-  click on the icon **Measure**, then select **“Save Annotation”** from
   the list.

-  annotation saving window will appear:

|image113|

Figure - Save annotation window

-  The following information can be filled:

-  **Label** (required);

-  Description;

-  Creator name.

   -  enter the information.

   -  click **Save**.

   -  system saves annotation with the following information:

-  title;

-  description;

-  any drawn measurements;

-  written text.

-  creator name.

   -  once the annotation has been saved, the annotation mark will
      appear next to the study image.

**********************
**TO VIEW ANNOTATION**
**********************

-  if there are several annotations, user can choose which one to
   review.

-  in order to view the annotation, drag and drop the study image (the
   one that has the annotation mark) to the main screen and the
   annotation icon will appear on the image:

|image114|

Figure - Annotation icon

-  move your mouse cursor to the **Annotation** mark.

-  click on the icon and choose an annotation from the list:

|image115|

Figure - List of annotations

-  click on the annotation you have chosen to view and the saved
   annotation will appear on the screen with an information that has
   been saved previously (line measurement in this case):

|image116|

Figure - View annotation

**************
**KEY OBJECT**
**************

|image32|

   **Key Object** concept is used in order to mark most interesting
   instances and save them for later review. Marked instances as Key
   Objects are stored in DICOM file of KO modality. Instances from
   different series can be stored in one Key Object selection. All
   instances marked as key objects are annotated with small star symbol.

   Key Object button for selecting Key Object selections is associated
   with loaded study:

|image33|

Figure - “Key Object” function pop-up window

   Annotated instances that are marked as Key Objects but not saved:

|image34|

Figure - Key Object mark (1)

   Annotated instances that are marked as Key Objects and are saved:

|image35|

Figure - Key Object mark (2)

   **Preload study/series** are intended for a preload of a study or
   series in order to scroll through the images much faster with a help
   of mouse wheel. Once you click the preload icon, preloading starts.

|image36|

Figure - Preload progress

|image37|

Figure - Preload study/series

   This function preloads study and series data. The stored series can
   be scrolled interactively in the form of scrollable image stacks.
