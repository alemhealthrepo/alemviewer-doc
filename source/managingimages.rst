﻿
###############
MANAGING IMAGES
###############


*******************
**PATIENT HISTORY**
*******************

|image38|

   **Patient history** forwards you to patient’s study history.

-  The patient history window is available in the main study window
   along with the patient information by clicking the history button.

-  Once you click on the patient history button it opens window that
   displays all patient studies that is available through the AlemHealth
   WEB DICOM Viewer.

-  The following information is provided: Modality, Description, Date &
   time.

|image39|

Figure - Patient history pop-up search window

-  Patient history list is filtered by the type of the study (modality).

|image40|

Figure - Patient history filter

-  When **All** modality type filter is chosen, the list provides all
   types of patient studies for the study detections using the Patient
   ID.

-  Sorting patient study history list is possible with all columns in
   the list. To sort the list according to the selected column, you need
   to click the sort button in the heading of the selected column.

|image41|

Figure - Patient history sorting button

-  The search in the patient study history window is available in all
   columns in the list. To search, enter the search phrase into the
   column header in the search field of the selected column and click on
   the search button.

|image42|

Figure - Patient history search according to the search phrase

-  The selected study in the patient study history window can be opened
   by clicking on the eye icon in the first column of the study list. To
   open multiple studies at a time, select all the studies that you want
   to open and click the **Add studies to viewer** button.

-  All open studies are highlighted in the provided list by placing the
   green indicator **Added**.

|image43|

Figure - Patient history added study

    Patient history search is performed according to the Patient ID.

**********
**EXPORT**
**********

|image108|

   To **Export** the study (to burn it on a CD/DVD or save it on your
   computer):

-  select or open the study that you want to write on the CD or DVD and
   click **Export**:

-  the export window appears:

|image109|

Figure - Export menu

   To **Export** the study (**to burn it on a CD**):

-  choose CD, DVD or Unlimited.

-  click **Burn**.

..

   After a while two buttons **Download ISO** and **Burn Now** will
   appear for every created volume. Click **Download ISO** in order to
   download a disk image with the .iso file extension, and burn it with
   your favorite CD/DVD burner software. Click **Burn Now**

   To **Export** the study (**to save its archive**):

-  choose the format, then select to save an image, a series of images
   or an active study.

-  click **Save** and choose a folder where you prefer to save the
   images in your computer. Click **Save** again.

*******************
**PRINTING IMAGES** 
*******************

=====
PRINT
=====

|image117|

   To print images, click **Print** button, which is in the middle of
   the Menu bar (enabled for images and disabled for videos, ECG and SR
   documents).

|image118|

Figure - Print options

   Click on one of Print options (**Print Active Layout Area** or
   **Print Non-Empty Layout Areas**) in order to print the selected
   image area view.


***********
**SR VIEW**
***********

   SR view enables to view structured reports.

|image134|

Figure - SR window

|image135|

Figure - SR window

   SR window displays standard DICOM Structured Reports.

************
**PDF VIEW**
************

   PDF view enables to view PDF files encapsulated in DICOM format.

|image136|

Figure - PDF window

|image137|

Figure - PDF display

   PDF window displays a standard PDF reader. Some Web browsers have
   built-in readers, in other cases the workplace needs additional
   software like Adobe Acrobat Reader.

**************
**VIDEO VIEW**
**************

   Software enables to view video files, MPEG2 and MPEG4 (H.264),
   encapsulated in DICOM format.

|image138|

Figure - Video player

   Video is played with the standard video player available.

*********
**DICOM**
*********

|image26|

   **DICOM** button is used to show DICOM tags of active window screen.
   Search according to Value, Type, Name and Tag is possible.

|image27|

Figure - DICOM tag window


