##################
IMAGE LOCALIZATION
##################

********
**LINK**
********

|image44|

**Link** button is intended for making comparison on image slice
location. There are three types of this button: **Automatic**,
**Manual** and **Disabled**.

+-----------------------------------+-----------------------------------+
|    |image48|                      |    **Automatic** mode             |
|                                   |    synchronizes different series  |
|                                   |    according to image             |
|                                   |    orientation, image position    |
|                                   |    and slice thickness            |
|                                   |    automatically. Series          |
|                                   |    consisting of images that have |
|                                   |    reference to the same UID      |
|                                   |    within the same study are      |
|                                   |    automatically synchronized by  |
|                                   |    default. Tap the icon once in  |
|                                   |    order to enable the Automatic  |
|                                   |    mode.                          |
+===================================+===================================+
|    |image49|                      |    **Manual** mode allows to      |
|                                   |    compare the images from the    |
|                                   |    study series manually. Series  |
|                                   |    acquired in similar planes     |
|                                   |    belonging to the same or       |
|                                   |    different studies (of the same |
|                                   |    or different patients) can be  |
|                                   |    synchronized manually by       |
|                                   |    default. Tap the icon twice in |
|                                   |    order to enable the Manual     |
|                                   |    mode.                          |
+-----------------------------------+-----------------------------------+
|    |image50|                      |    **Disabled** mode disables     |
|                                   |    synchronization modes.         |
+-----------------------------------+-----------------------------------+

******************
**REFERENCE LINE**
******************

|image101|

   Overlaying reference lines allow you to indicate the location of an
   image slice on another image of an intersecting pane.

-  Select the images that you want to compare and move them into the
   panes:

-  Select one of the image you want to know the location of in regard to
   other images.

-  Click on that selected pane.

-  Click the button **Reference Line.**

-  Yellow lines appear in the images, indicating the location of the
   selected image:

|image102|

Figure - Reference line option

-  Scroll down the mouse wheel in order to select another slice
   downwards, and vice versa.
