#############
SCREEN LAYOUT
#############


**********
**LAYOUT**
**********

|image13|

   **Layout** button divides the screen into sections and allows you to
   drag as many images as you want to the right side of the screen. It
   helps in comparing images:

|image14|

***************
**MULTI IMAGE**
***************

|image15|

   For example, **2x2** button on **Multi image** drop down list divides
   the selected section into 4 subsections. Once you have selected this
   button, drag the studies to the field. The study and all the
   following images that you want will appear on the selected field.

   All image manipulation functions affect the entire set of images
   opened in a multiple viewports mode (such as **Scroll, Windowing,
   Rotate, Pan, Reset**). For example, if you select **Bone** contrast
   mode it will apply the **Bone** mode to all images that are viewed
   through the multiple viewports mode though the changes do not apply
   to the image which is not viewed via multiple viewports.

|image16|

Figure - Multiple viewports

**************
**FULLSCREEN**
**************

|image29|

   **Full Screen** button. Move your mouse cursor to the upper right
   corner of the screen. Click on the Full Screen icon and the Full
   Screen mode will be enabled. Click either the icon once again or ESC
   button at your keyboard in order to exit the Full Screen Mode.
