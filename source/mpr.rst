﻿#################################
Multi-Planar Reconstruction (MPR)
#################################

************
**MPR VIEW**
************

|image19|

   Selecting the **MPR** view is done by clicking the MPR button in the
   views panel. It contains three different panels:

-  Multiplanas

-  Axial

-  Sagittal

-  Coronal

|image20|

Figure - MPR selection

   After you click on the **MPR** button, the loading will start.

|image21|

   *Figure 35..*

   Once the loading process is done, you will be able to scroll the
   mouse wheel up and down over the image and see the view (axial,
   sagittal, coronal) you have selected.

*********************
**CROSSHAIR TOOLTIP**
*********************

|image22|

   **Crosshair-tooltip** is mainly used on MPR studies. A crosshair
   represents the intersecting planes of the selected point on the main
   study (RGB – Red, Green, Blue).

|image23|

   *Figure 36..*
