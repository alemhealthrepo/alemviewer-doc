﻿##########
ECG MODULE
##########

   This module allows you to view DICOM ECG wave data.

|image119|

Figure - ECG view

   When viewing ECGs, behavior is different:

-  Measurement tools are changed into ECG measurement tools.

|image120|

Figure - ECG measurements

-  Image manipulation buttons are disabled.

***************
**MEASUREMENT**
***************

|image121|

   The **Measurement** button is used to measure fragment length in
   seconds, mV and calculate heart rate (BPM). To measure:

-  Select **Measurement**.

-  Move the mouse cursor on the point you want.

-  Click down and move mouse over an ECG wave.

|image122|

Figure - Measurements

*************
**QT POINTS**
*************

|image123|

   The **QT points** button is used to measure wave intervals RR, QT and
   QTc. To measure:

-  Select **QT points**.

-  Move the mouse cursor on the point you want to set Q point and click.

-  Move the mouse cursor on the point you want to set T point and click.

|image124|

Figure - QT points

-  Move the mouse cursor on the point you want to set the next Q point
   and click (double click also works).

******
**HR**
******

|image125|

   The button **HR** is designated to measure heart rate and visually
   estimate its irregularity:

-  Select **HR** measurement tool;

-  Move the mouse cursor on the point you want to set R point and click
   once the left mouse button;

-  Move the mouse cursor on the point you want to set next R point and
   click once the left mouse button;

-  Now you can compare given interval with other R points.

|image126|

Figure - HR measurement tool

************
**QRS AXIS**
************

|image127|

   The **QRS Axis** is used to measure cardiac interventricular
   partition and ventricular depolarization spreading.

-  Select „QRS axis“ measurement tool;

-  Move the mouse cursor on the point you want to start your “QRS”
   measurement (“Q” point) and click once the left mouse button;

-  Move the mouse cursor on the point you want to end your “QRS”
   measurement ("S“ point) and click once the left mouse button.

|image128|

Figure - QRS Axis measurement tool

********************
**HORIZONTAL SCALE**
********************

|image129|

   Change horizontal scale (mm per second).

******************
**VERTICAL SCALE**
******************

|image130|

   Change vertical scale (mm per mV).

.. _pan-1:

*******
**PAN**
*******

|image131|

   **Pan** button to adjust ECG data position.

.. _zoom-1:

********
**ZOOM**
********

|image132|

   **Zoom** button to adjust ECG data zoom.

-  When you click **Fit to Screen** button, the size of the image is
   automatically adjusted so that the image would fill the entire
   screen. For example, if only part of the plot is visible on the
   screen, choose this button to see the whole ECG plot displayed on the
   entire screen.

-  When you click **Original resolution** button, the size of the image
   changes into original size.

**********
**FILTER**
**********

|image133|

   **Filter** function is used for the following:

-  trims the edges of unnecessary points (points to the first spike that
   has no importance);

-  trims high and low frequency signals applying low–pass and high–pass
   frequency filters under the “Filter Low Frequency“ (003A,0220) and
   “Filter High Frequency“ (003A,0221) tags;

-  eliminates baseline wandering interference;

-  filters out specified frequency signals adjusting band-stop filter by
   **Notch Filter Frequency** (003A, 0222) tag.
